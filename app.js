const http = require('http');
const url = require('url');
const fs = require('fs');


http.createServer(function (req, res) {
	let q = url.parse(req.url, true);
	let filename = "." + q.pathname;

	if (filename != "./") {
		fs.readFile(filename, function (err, data) {
			if (err) {
				res.writeHead(404, { 'Content-Type': 'text/html' });
				res.end("404 Not Found");
				return
			}
			res.writeHead(200, { 'Content-Type': 'text/html' });
			res.write(data);
			res.end();
			return
		});
	} else {
		res.writeHead(200, { 'Content-Type': 'text/html' });
		res.write("Unknown Request");
		res.end();
	}

}).listen(8888);


