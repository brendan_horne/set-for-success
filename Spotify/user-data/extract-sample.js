const fs = require("fs");
var top = JSON.parse(
  fs.readFileSync("Spotify/user-data/top-100-long-term.json")
);

function getArtistAndSong(top) {
  let list = [];
  for (i = 0; i < top.items.length; i++) {
    list.push({
      artist: top.items[i].artists[0].name,
      song: top.items[i].name
    });
  }
  fs.writeFileSync(
    "Spotify/user-data/list.json",
    JSON.stringify(list, null, 1)
  );
  return;
}

getArtistAndSong(top);
